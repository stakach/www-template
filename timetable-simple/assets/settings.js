/**
 * ROOT SETTIGNS
 */
var settings = {
    debug: true,
    composer: {},
    app: {},
    mock: undefined
};
/**
 * COMPOSER SETTINGS
 */
settings.composer = {
    domain: '',
    route: '',
    protocol: null,
    port: '',
    use_domain: false,
    local_login: false
};
/**
 * ROOT APPLICATION SETTINGS
 */
settings.app = {
    title: 'ACA Projects',
    description: 'ACA Staff UI written with Angular Framework',
    short_name: 'STAFF',
    logo_dark: {
        type: 'img',
        src: 'assets/img/logo.svg',
        background: ''
    },
    logo_light: {
        type: 'img',
        src: 'assets/img/logo-inverse.svg',
        background: ''
    },
    booking: {}
};
/**
 * BOOKING FORM SETTINGS
 */
settings.app.booking = {
    title_prefix: 'Panel Booking',
    show_fields: ['organiser', 'date', 'duration', 'title'],
    required_fields: ['organiser']
};
// Add settings to global space
window['settings.json'] = settings;
console.log('Embedded setting.json');
