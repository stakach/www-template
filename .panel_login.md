# Usage of panel_login.html

This static html file is for use with public endpoints only (e.g. booking panels, kiosks)

1) Create a role based account using backoffice. This must be done while accessing backoffice from the same domain that the kiosk device will be signing into the app. E.g. kiosk@aca.im with a long random password
2) https://www.base64encode.net and encode (without the quotes) `u=kiosk@aca.im&p=a-long-random-password`
3) The final url which should be configured on the devices is: https://rhv.aiauat.biz/panel_login.html?u=dT1raW9za0BhY2EuaW0mcD16ZXFjYXNk&continue=/bookings/